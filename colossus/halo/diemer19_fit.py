###################################################################################################
#
# concentration.py          (c) Benedikt Diemer
#     				    	    diemer@umd.edu
#
###################################################################################################

"""
This module implements a range of models for halo concentration as a function of mass, redshift,
and cosmology.

---------------------------------------------------------------------------------------------------
Basics
---------------------------------------------------------------------------------------------------

The main function in this module, :func:`concentration`, is a wrapper for all models::

	from colossus.cosmology import cosmology
	from colossus.halo import concentration

	cosmology.setCosmology('planck15')
	cvir = concentration.concentration(1E12, 'vir', 0.0, model = 'bullock01')

Alternatively, the user can also call the individual model functions directly. However, there are
two aspects which the :func:`concentration` function handles automatically. First, many
concentration models are only valid over a certain range of masses, redshifts, and
cosmologies. If the user requests a mass or redshift outside these ranges, the function returns a
*soft fail*: the concentration value is computed, but a warning is displayed and/or a False flag
is returned in a boolean array. If a concentration model cannot be computed, this leads to a *hard
fail* and a returned value of ``INVALID_CONCENTRATION`` (-1).

Second, each model was only calibrated for one of a few particular mass definitions, such as
:math:`c_{200c}`, :math:`c_{vir}`, or :math:`c_{200m}`. The :func:`concentration` function
automatically converts these definitions to the definition chosen by the user (see :doc:`halo_mass`
for more information on spherical overdensity masses). For the conversion, we necessarily have to
assume a particular form of the density profile (see the documentation of the
:func:`~halo.mass_defs.changeMassDefinition` function).

.. note::
	The conversion to other mass definitions can degrade the accuracy of the predicted
	concentration by up to ~15-20% for certain mass definitions, masses, and redshifts. Using
	the DK14 profile (see the :mod:`halo.profile_dk14` module) for the mass conversion gives
	slightly improved results, but the conversion is slower. Please see Appendix C in
	`Diemer & Kravtsov 2015 <http://adsabs.harvard.edu/abs/2015ApJ...799..108D>`_ for details.

.. note::
	The user must ensure that the cosmology is set consistently. Many concentration models were
	calibrated only for one particular cosmology (though the default concentration model,
	``diemer15``, is valid for all masses, redshifts, and cosmologies). Neither the
	:func:`concentration` function nor the invidual model functions issue warnings if the set
	cosmology does not match the concentration model (with the exception of the
	:func:`modelKlypin16fromM` and :func:`modelKlypin16fromNu` functions). For example, it is
	possible to set a WMAP9 cosmology, and evaluate the Duffy et al. 2008 model which is only
	valid for a WMAP5 cosmology. When using such models, it is the user's responsibility to ensure
	consistency with other calculations.


The original version of the ``diemer15`` model suffered from a small numerical error, a corrected
set of parameters is given in
`Diemer & Joyce 2019 <https://ui.adsabs.harvard.edu//#abs/2018arXiv180907326D/abstract>`_. The differences
between the models are less than 5%, but the original model should be used only for the purpose
of backwards compatibility.
"""

###################################################################################################

import numpy as np
import scipy.integrate
import scipy.interpolate
import scipy.optimize
import warnings
from collections import OrderedDict

from colossus.utils import utilities
from colossus.utils import constants
from colossus.utils import storage
from colossus import defaults
from colossus.cosmology import cosmology
from colossus.lss import peaks
from colossus.halo import mass_so
from colossus.halo import mass_defs
from colossus.halo import profile_nfw
from colossus.halo import profile_einasto

###################################################################################################

###################################################################################################
# STORAGE SYSTEM
###################################################################################################

storageUser = None

def _getName():
	return "concentration"

def _getHash():
	return "concentration"

def _getStorageUser():

	global storageUser
	if storageUser is None:
		storageUser = storage.StorageUser('halo.concentration', 'rw', _getName, _getHash, None)

	return storageUser

###################################################################################################

# The effective exponent of linear growth, the logarithmic derivative of the growth factor.

def _diemer19_alpha_eff(z):

	cosmo = cosmology.getCurrent()
	D = cosmo.growthFactor(z, derivative = 0)
	dDdz = cosmo.growthFactor(z, derivative = 1)
	alpha_eff = -dDdz * (1.0 + z) / D

	return alpha_eff

###################################################################################################

# This function is a generalized version of the Diemer & Joyce 2019 model, where the user can
# pass a set of parameters and give mass in any definition.

###################################################################################################

def mu_einasto(x):
    alpha = 0.17
    integrand = lambda xx: xx * xx * np.exp(-2. / alpha * (xx**alpha))
    result = np.zeros((x.shape[0]))
    for i in range(x.shape[0]):
        result[i] += np.exp(2./alpha) * scipy.integrate.quad(integrand, 0., x[i])[0]
    return result

def mu_lazar(x):
    beta = 0.3
    integrand = lambda xx: xx * np.exp(-1. / beta * xx**beta)
    result = np.zeros((x.shape[0]))
    for i in range(x.shape[0]):
        result[i] += np.exp(1./beta) * scipy.integrate.quad(integrand, 0., x[i])[0]
    return result

def _diemer19_general(M, z, a0, a1, b0, b1, c_alpha, kappa):

	# ---------------------------------------------------------------------------------------------

	# Currently, only NFW predictions are implemented. In principle, the model can predict
	# c based on other mass profiles, but the parameters would need to be re-tuned. The
	# implementation of the Einasto profile below is for test purposes only.

        #profile = 'nfw'
	profile = 'einasto'
	#profile = 'lazar'

        params = {}
        params['a_0'] = a0; params['a_1'] = a1
        params['b_0'] = b0; params['b_1'] = b1
        params['c_alpha'] = c_alpha
        params['kappa'] = kappa

        ps_args = defaults.PS_ARGS

	# ---------------------------------------------------------------------------------------------

	def getTableName(profile):

		if profile == 'nfw':
			table_name = 'diemer19_'
		elif profile == 'einasto':
			table_name = 'diemer19_einasto_'
		elif profile == 'lazar':
			table_name = 'diemer19_lazar_'
		else:
			raise Exception('Unknown profile type, %s. Valid are "nfw" or "einasto".' % (profile))

		return table_name

	# ---------------------------------------------------------------------------------------------
	# The G(c) inverse function that needs to be mumerically inverted is tabulated, the table
	# inverted to give c(G, n). This is a little tricky because G(c) has a minimum that depends on
	# n.

	def computeGcTable(profile):

		n_G = 80
		n_n = 40
		n_c = 80

		n = np.linspace(-4.0, 0.0, n_n)
		c = np.linspace(-1.0, 3.0, n_c)

		# The left hand side of the equation in DJ19
		lin_c = 10**c

		if profile == 'nfw':
			mu = profile_nfw.NFWProfile.mu(lin_c)
		elif profile == 'einasto':
			mu = mu_einasto(lin_c)
		elif profile == 'lazar':
			mu = mu_lazar(lin_c)
		else:
			raise Exception('Unknown profile type, %s. Valid are "nfw" or "einasto".' % (profile))

		lhs = np.log10(lin_c[:, None] / mu[:, None]**((5.0 + n) / 6.0))

		# At very low concentration and shallow slopes, the LHS begins to rise again. This will cause
		# issues with the inversion. We set those parts of the curve to the minimum concentration of
		# a given n bin.
		mask_ascending = np.ones_like(lhs, np.bool)
		mask_ascending[:-1, :] = (np.diff(lhs, axis = 0) > 0.0)

		# Create a table of c as a function of G and n. First, use the absolute min and max of G as
		# the table range
		G_min = np.min(lhs)
		G_max = np.max(lhs)
		G = np.linspace(G_min, G_max, n_G)

		gc_table = np.ones((n_G, n_n), np.float) * -10.0
		mins = np.zeros_like(n)
		maxs = np.zeros_like(n)
		for i in range(n_n):

			# We interpolate only the ascending values to get c(G)
			mask_ = mask_ascending[:, i]
			lhs_ = lhs[mask_, i]
			mins[i] = np.min(lhs_)
			maxs[i] = np.max(lhs_)
			interp = scipy.interpolate.InterpolatedUnivariateSpline(lhs_, c[mask_])

			# Not all G exist for all n
			mask = (G >= mins[i]) & (G <= maxs[i])
			res = interp(G[mask])
			gc_table[mask, i] = res

			mask_low = (G < mins[i])
			gc_table[mask_low, i] = np.min(res)
			mask_high = (G > maxs[i])
			gc_table[mask_high, i] = np.max(res)

		# Store the objects using the storage module. An interpolator will automatically be
		# created.
		storageUser = _getStorageUser()
		table_name = getTableName(profile)

		object_data = (G, n, gc_table)
		storageUser.storeObject('%sGc' % table_name, object_data = object_data, persistent = True)
		object_data = np.array([n, mins])
		storageUser.storeObject('%sGmin' % table_name, object_data = object_data, persistent = True)
		object_data = np.array([n, maxs])
		storageUser.storeObject('%sGmax' % table_name, object_data = object_data, persistent = True)

		return

	# ---------------------------------------------------------------------------------------------
	# Try to load the interpolators from storage. If they do not exist, create them.

	def getGcTable(profile):

		storageUser = _getStorageUser()
		table_name = getTableName(profile)

		interp_Gc = storageUser.getStoredObject('%sGc' % table_name, interpolator = True,
											store_interpolator = True)
		if interp_Gc is None:
			computeGcTable(profile)
			interp_Gc = storageUser.getStoredObject('%sGc' % table_name, interpolator = True,
											store_interpolator = True)

		interp_Gmin = storageUser.getStoredObject('%sGmin' % table_name, interpolator = True,
											store_interpolator = True)
		interp_Gmax = storageUser.getStoredObject('%sGmax' % table_name, interpolator = True,
											store_interpolator = True)

		# These interpolators are created at the same time as the Gc interpolator. If they do not
		# exist, something went wrong.
		if interp_Gmin is None or interp_Gmax is None:
			raise Exception('Loading table for diemer19 concentration model failed.')

		return interp_Gc, interp_Gmin, interp_Gmax

	# ---------------------------------------------------------------------------------------------

	# Compute peak height, n_eff, and alpha_eff
	nu = peaks.peakHeight(M, z, ps_args = ps_args)
	n_eff = peaks.powerSpectrumSlope(nu, z, slope_type = 'sigma', scale = params['kappa'], ps_args = ps_args)
	alpha_eff = _diemer19_alpha_eff(z)

	is_array = utilities.isArray(nu)
	if not is_array:
	        nu = np.array([nu])
		n_eff = np.array([n_eff])
		alpha_eff = np.array([alpha_eff])

	# Compute input parameters and the right-hand side of the c-M equation. We use interpolation
	# tables to find the concentration at which the equation gives that RHS.
	A_n = params['a_0'] * (1.0 + params['a_1'] * (n_eff + 3.0))
	B_n = params['b_0'] * (1.0 + params['b_1'] * (n_eff + 3.0))
	C_alpha = 1.0 - params['c_alpha'] * (1.0 - alpha_eff)
	rhs = np.log10(A_n / nu * (1.0 + nu**2 / B_n))

	# Get interpolation table
	interp_Gc, interp_Gmin, interp_Gmax = getGcTable(profile)

	# Mask out values of rhs for which do G not exist. The interpolator will still work because it
	# is based on a full grid of G values, but we mask out the invalid array elements.
	mask = (rhs >= interp_Gmin(n_eff)) & (rhs <= interp_Gmax(n_eff))
	c = 10**interp_Gc(rhs, n_eff, grid = False) * C_alpha
	c[np.logical_not(mask)] = INVALID_CONCENTRATION

	if not is_array:
		c = c[0]
		mask = mask[0]

	return c, mask

###################################################################################################
